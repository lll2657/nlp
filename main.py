from flask import Flask, render_template, jsonify, request
from flask_cors import CORS
import requests
import pymongo
import json
import bson,sys,itertools

app = Flask(__name__,
            static_folder = "./dist/static",
            template_folder = "./dist")
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})

@app.route('/api/query')
def query():
	#Connect to MongoDB
	#client=pymongo.MongoClient('localhost',27017)
	client=pymongo.MongoClient('mongodb+srv://farzad:caltex@dictionary-jwce8.gcp.mongodb.net/test?retryWrites=true')
	db=client.nlp
	collection = db.DictData

	query = json.loads(request.args.get('statement'))

	cursor = collection.find(query).skip(int(request.args.get('skip'))*50).limit(50)

	results=[]
	for doc in cursor:	
	    x=list(doc.values())
	    results.append(str(doc["_id"]["Instant_ID"]))
	    results.append(x[2])

	client.close()
	#output=results
	output=dict(itertools.zip_longest(*[iter(results)] * 2, fillvalue=""))
	return jsonify(output)

@app.route('/api/search')
def search():
	#Connect to MongoDB
	#client=pymongo.MongoClient('localhost',27017)
	client=pymongo.MongoClient('mongodb+srv://farzad:caltex@dictionary-jwce8.gcp.mongodb.net/test?retryWrites=true')
	db=client.nlp
	collection = db.DictData

	query = json.loads(request.args.get('statement'))
	type = request.args.get('type')
	value = request.args.get('value')

	cursor = collection.find(query)

	temp = []
	results = []

	for doc in cursor:
		if len(results) == 10:
				break
		for el in doc["Tokenization"]:
			if el[type].startswith(value):
				temp.append(el[type])
				results = list(set(temp))
			if len(results) == 10:
				break

	results = sorted(results)

	return jsonify(results)


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    return render_template("index.html")
