import Vue from 'vue'
import App from './App'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import vClickOutside from 'v-click-outside'
import 'vue-awesome/icons'

Vue.use(BootstrapVue)
Vue.use(vClickOutside)
Vue.config.productionTip = false

/* App scss */
import './assets/style/app.scss'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
